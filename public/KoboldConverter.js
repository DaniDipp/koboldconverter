var ALPHABET = [" ","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","Yes","No","Maybe","Okay","The"];
var KFLAG = ["Kobold","1","2","3","4","5","6","7","8","9","0","th","er","on","an","re","he","in","rawb","Megan","Gengar","Noojuno","Meta","Stream","Chat","Meme","Sneaky Mouse","Danger Rat","Cheese","Epic","Radical","Golden"];

function dekoboldify(sentence){
    sentence = sentence.split(/\b/); //split at word boundaries: "kOBOld. kobold koBolD' koBOlD kobold KObold KoBOLD!" -> ["kOBOld", ". ", "kobold", " ", "koBolD", "' ", "koBOlD", " ", "kobold", " ", "KObold", " ", "KoBOLD", "!"]
    var fullSentence = "";
    for(var word of sentence){ //for each part of the sentence
        if(word.toLowerCase() == "kobold"){ //check if this part is "kobold". Only changed to lower case for this comparison
            var index = koboldToInt(word.substring(1)); //convert "kobold" to integer (without K flag)
            if(word.charAt(0) !== word.charAt(0).toUpperCase()){ //check if K flag is set
                fullSentence += ALPHABET[index]; //get character from ALPHABET if it isn't
            }else{
                fullSentence += KFLAG[index]; //get character from KFLAG if it isn't
            }
        }else{ fullSentence += word.trim(); } //if part is not "kobold", it's a symbol. trim spaces away and add
    }
    return fullSentence;
}

function koboldToInt(word){
    var index = ""; //set up string to store bits
    for(var i=0; i<word.length; i++){ //go every character
        if(word.charAt(i) == word.charAt(i).toUpperCase()){
            index += "1"; //add 1 to the end, if character is upper case
        }else {
            index += "0"; //add 0 to the end, if character is upper case
        }
    }
    return parseInt(index, 2) //parse binary string to int
}

function koboldify(sentence){
    sentence = sentence.toLowerCase(); //convert everything to lower case.
    var kSentence = ""; //add kobolds to this variable
    while(sentence.length>0){ //repeat until sentence is empty
        var found = false;
        for(var i=KFLAG.length-1; i>=0; i--){ //start at the end for consistency
            if(KFLAG[i] === "") continue; //skip empty entries
            if(sentence.startsWith(KFLAG[i].toLowerCase())){ //entry matches start of sentence
                kSentence += " K"+intToKobold(i); //convert current index to kobold, add K-flag (and space)
                sentence = sentence.substring(KFLAG[i].length); //remove the found entry from the start of sentence
                found = true; //prevent next loop from starting before this loop runs through completely
                break; //hop out of loop
            }
        }
        if(!found){
            for(var i=ALPHABET.length-1; i>=0; i--){ //start at the end because single letters are in the front
                if(ALPHABET[i] === "") continue; //skip empty entries
                if(sentence.startsWith(ALPHABET[i].toLowerCase())){ //entry matches start of sentence
                    kSentence += " k"+intToKobold(i); //convert current index to kobold, add k-flag (and space)
                    sentence = sentence.substring(ALPHABET[i].length); //remove the found entry from the start of sentence
                    found = true; //prevent catch-all from running before this loop runs through completely, too
                    break; //hop out of loop
                }
            }
        }
        if(!found){ //if nothing matches, move the first character over as-is.
            kSentence += sentence.charAt(0);
            sentence = sentence.substring(1);
        }
    }
    return kSentence.trim(); //remove leading/trailing spaces
}

function intToKobold(number){
    var obold = "obold"; //string to modify
    var obold2 = ""; //string to add to
    var index = ("00000"+number.toString(2)); //convert int to binary string, with 5 leading zeroes
    index = index.substring(index.length-5); //trim leading zeroes to 5 digits total
    for(var i=0; i<index.length; i++){ //loop through digits of index string
        if(index.charAt(i)==1){ //check if current index digit is 1
            obold2 += obold.charAt(i).toUpperCase(); //convert character at current position to upper case
        }else{
            obold2 += obold.charAt(i); //just add it if current digit is 0
        }
    }
    return obold2; //return the whole thing
}
